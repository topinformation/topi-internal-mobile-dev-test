# TOPi - Mobile Developer Test

## Goal
- To develop a web application able to display a list of searched meals on TheMealDB.
- The API example: https://www.themealdb.com/api/json/v1/1/search.php?s=
- If you are testing for an iOS position, you can use Swift and/or Objetive-C. For Android, use Java and/or Kotlin.

# Reference layout:

![main](https://i.ibb.co/LrYp945/Microsoft-Teams-image-23.png)
![detail](https://i.ibb.co/MhWY1h7/Microsoft-Teams-image-20.png)

## Required
- The layout must work on any iPhone/Android in portrait mode
- JSON Mapping -> Object
- At least one unit test
- Create a search filter in the toolbar (using material design pattern on Android) which filter the list with any sub-string of the meal
- Give the user the option to sort the list by: name
- Meal detail screen
- MVVM

## Extra Points
- Image caching
- UI testing
- Use gitflow
- Android extra points: Constraint Layout, JAVA 8 lambda, Card View for list rows, dagger 2